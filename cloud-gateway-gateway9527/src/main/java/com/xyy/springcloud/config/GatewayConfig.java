package com.xyy.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

import java.time.ZonedDateTime;

/**
 * @author xyy
 * @date 2021年06月28日 13:54
 */
@Configuration
public class GatewayConfig {

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        RouteLocator baidu = routes.route("baidu_guonei", r -> r.path("/guonei").uri("http://news.baidu.com/guonei")).build();
        return baidu;
    }

    @Bean
    public RouteLocator customRouteLocator2(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        RouteLocator baidu = routes.route("baidu_guoji",
                r -> r.path("/guoji")
                        .and().before(ZonedDateTime.parse("2021-06-28T15:13:42.198+08:00[Asia/Shanghai]"))
                        .and().method(HttpMethod.GET)
                        .uri("http://news.baidu.com/guoji")).build();
        return baidu;
    }
//    @Bean
//    public RouteLocator customRouteLocator3(RouteLocatorBuilder routeLocatorBuilder){
//        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
//        RouteLocator baidu = routes.route("port",r -> r.path("/payment/port").uri("lb://cloud-payment-service")).build();
//        return baidu;
//    }
}
