package com.xyy.springcloudalibaba.controller;

/**
 * @author xyy
 * @date 2021年07月07日 13:37
 */

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class CircleBreakerController {
    public static final String SERVICE_URL = "http://nacos-payment-provider";

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/consumer/fallback/{id}")
    //@SentinelResource(value = "fallback")//没有配置
    //@SentinelResource(value="fallback",fallback = "handlerFallback") //运行异常 降级方法
    //@SentinelResource(value = "fallback",blockHandler = "blockHandler")     //sentinel 控制台配置违规
    @SentinelResource(value = "fallback", fallback = "handlerFallback", blockHandler = "blockHandler",
            exceptionsToIgnore = {IllegalArgumentException.class})
    public Output<Payment> fallback(@PathVariable Long id) {
        Output<Payment> result = restTemplate.getForObject(SERVICE_URL + "/paymentSQL/" + id, Output.class, id);

        if (id == 4) {
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常....");
        } else if (result.getData() == null) {
            throw new NullPointerException("NullPointerException,该ID没有对应记录,空指针异常");
        }
        return result;
    }

    //Throwable 可以将报错信息带过来
    public Output handlerFallback(Long id, Throwable e) {
        return Output.failure(444, "兜底异常 handlerFallback,exception内容    id:" + id + "    " + e.getMessage());
    }

    public Output blockHandler(Long id, BlockException e) {
        return Output.failure(444, "兜底异常 blockHandler,exception内容   id:" + id + "   " + e.getMessage());
    }
}

