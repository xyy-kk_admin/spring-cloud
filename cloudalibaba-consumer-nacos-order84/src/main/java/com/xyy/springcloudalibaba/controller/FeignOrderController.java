package com.xyy.springcloudalibaba.controller;

import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import com.xyy.springcloudalibaba.service.PaymentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年07月07日 15:18
 */
@RestController
@RequestMapping("/consumer/feign")
public class FeignOrderController {
    @Resource
    public PaymentService paymentService;

    @GetMapping("/getSQL/{id}")
    public Output<Payment> paymentSQL(@PathVariable("id") Long id) {
        return paymentService.paymentSQL(id);
    }
}
