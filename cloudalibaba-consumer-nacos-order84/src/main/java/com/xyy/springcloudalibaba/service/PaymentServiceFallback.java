package com.xyy.springcloudalibaba.service;

import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import org.springframework.stereotype.Component;

/**
 * @author xyy
 * @date 2021年07月07日 15:15
 */
@Component
public class PaymentServiceFallback implements PaymentService {

    @Override
    public Output<Payment> paymentSQL(Long id) {
        return Output.failure(400, "openfeign服务降级返回-->> PaymentServiceFallback");
    }
}
