package com.xyy.springcloudalibaba.service;

import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author xyy
 * @date 2021年07月07日 15:13
 */
@FeignClient(value = "nacos-payment-provider", fallback = PaymentServiceFallback.class)
public interface PaymentService {

    @GetMapping(value = "/paymentSQL/{id}")
    Output<Payment> paymentSQL(@PathVariable("id") Long id);
}
