package com.xyy.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xyy
 * @date 2021年06月30日 15:32
 */
@RestController
@RefreshScope
@RequestMapping("configclient")
public class ConfigClientController {
    //@Value("${xxx}")获取的是配置文件中的属性值,
    //因为没有配置config.info属性,而配置了云配置中心,所以访问的将是3344(13.2Config 配置 server 端)所写的config-dev.yml文件,此文件中有config.info属性
    //为什么访问config-dev.yml?   因为bootstrap.yml配置类config的label,name,profile三个属性,
    @Value("${config.info}")
    private String configInfo;

    @Value("${server.port}")
    private String port;

    @GetMapping("/configInfo")
    public String getConfigInfo() {
        return "port:  " + port + "  configInfo:  " + configInfo;
    }
}
