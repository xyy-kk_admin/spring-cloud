package com.xyy.springcloud.service.Impl;

import com.xyy.springcloud.service.IPaymentHystrixServiceImpl;
import org.springframework.stereotype.Component;

/**
 * @author xyy
 * @date 2021年06月24日 13:00
 */
@Component
public class PaymentHystrixServiceImplImpl implements IPaymentHystrixServiceImpl {

    @Override
    public String paymentInfo_OK(Integer id) {
        return "---------PaymentHystrixServiceImpl paymentInfo_OK ,(*^▽^*))";
    }

    @Override
    public String paymentInfo_timeout(Integer id) {
        return "---------PaymentHystrixServiceImpl paymentInfo_timeout ,o(╥﹏╥)o";
    }
}
