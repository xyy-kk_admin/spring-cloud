package com.xyy.springcloud.service;

import com.xyy.springcloud.service.Impl.PaymentHystrixServiceImplImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author xyy
 * @date 2021年06月23日 15:46
 */
@Component
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT", fallback = PaymentHystrixServiceImplImpl.class)
public interface IPaymentHystrixServiceImpl {
    final String path = "/payment/hystrix/";

    @GetMapping(path + "ok/{id}")
    public String paymentInfo_OK(@PathVariable("id") Integer id);

    @GetMapping(path + "timeout/{id}")
    public String paymentInfo_timeout(@PathVariable("id") Integer id);

}
