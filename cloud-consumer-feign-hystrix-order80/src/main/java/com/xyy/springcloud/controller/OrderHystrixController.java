package com.xyy.springcloud.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.xyy.springcloud.service.IPaymentHystrixServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年06月23日 15:47
 */
@RestController
@Slf4j
@RequestMapping("consumer/hystrix")
@DefaultProperties(defaultFallback = "payment_Global_FalllbackMethod")
public class OrderHystrixController {
    @Resource
    private IPaymentHystrixServiceImpl IPaymentHystrixServiceImpl;

    @GetMapping("ok/{id}")
    public String paymentInfo_OK(@PathVariable("id") Integer id) {
        return IPaymentHystrixServiceImpl.paymentInfo_OK(id);
    }


    @HystrixCommand(fallbackMethod = "paymentInfo_timeout2", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1500")
    })
    @GetMapping("timeout/{id}")
    public String paymentInfo_timeout(@PathVariable("id") Integer id) {
        return IPaymentHystrixServiceImpl.paymentInfo_timeout(id);
    }

    @HystrixCommand
    @GetMapping("timeout3/{id}")
    public String paymentInfo_timeout3(@PathVariable("id") Integer id) {
        log.info("======================");
        try {
            int age = 10 / 0;
        } catch (Exception e) {
            log.info("+++++++++++++++++++++++++++++++");
        }
        log.info("----------------------");
        return IPaymentHystrixServiceImpl.paymentInfo_timeout(id);
    }

    public String paymentInfo_timeout2(Integer id) {
        return "80端,支付繁忙,请稍后,请自检o(╥﹏╥)o" + id;
    }

    //全局fallback方法
    public String payment_Global_FalllbackMethod() {
        log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        return "payment_Global_FalllbackMethod    ";
    }
}
