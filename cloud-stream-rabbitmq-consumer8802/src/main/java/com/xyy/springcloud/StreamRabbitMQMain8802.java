package com.xyy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author xyy
 * @date 2021年07月01日 15:06
 */
@SpringBootApplication
@EnableEurekaClient
public class StreamRabbitMQMain8802 {
    public static void main(String[] args) {
        SpringApplication.run(StreamRabbitMQMain8802.class, args);
    }
}
