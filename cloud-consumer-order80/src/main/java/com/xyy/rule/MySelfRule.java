package com.xyy.rule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xyy
 * @date 2021年06月22日 15:31
 */
@Configuration
public class MySelfRule {
    @Bean
    public IRule myRule() {
        //return new RoundRobinRule();   // 轮巡,默认
        return new RandomRule();       // 定义为随机
        //return new RetryRule();        // 先轮巡,如果获取失败则重试其他服务
        //return new WeightedResponseTimeRule();  // 先轮巡,按照响应速度快排序
        //return new BestAvailableRule();     // 先过滤跳闸状态服务,在选择并发量最小服务
        //return new AvailabilityFilteringRule();     // 先过滤故障,在选择并发小的实例
        //return  new ZoneAvoidanceRule();    // 复合判断server所在区域的性能和server的可用性选择服务器
    }
}
