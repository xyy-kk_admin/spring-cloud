package com.xyy.springcloud.controller;

import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import com.xyy.springcloud.lb.LoadBalancer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

/**
 * @author xyy
 * @date 2021年06月18日 10:44
 */

@RestController
@Slf4j
@RequestMapping("/order/consumer")
public class OrderController {
    //public static final String PAYMENT_URL = "http://localhost:8001";
    public static final String PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private LoadBalancer loadBalancer;

    @Resource
    private DiscoveryClient discoveryClient;


    @GetMapping("create")
    public Output<Payment> create(Payment payment) {
        System.out.println("====================");
        return restTemplate.postForObject(PAYMENT_URL + "/payment/create", payment, Output.class);
    }

    @GetMapping("get/{id}")
    public Output<Payment> getPayment(@PathVariable("id") Long id) {
        System.out.println("--------------------");
        return restTemplate.getForObject(PAYMENT_URL + "/payment/get/" + id, Output.class);
    }

    @GetMapping("discovery")
    public Object discovery() {
        return restTemplate.getForObject(PAYMENT_URL + "/payment/discovery/", Object.class);
    }


    @GetMapping("getForEntity/{id}")
    public Output<Payment> getPayment1(@PathVariable("id") Long id) {
        ResponseEntity<Output> forEntity = restTemplate.getForEntity(PAYMENT_URL + "/payment/get/" + id, Output.class);
        //是否返回200成功码
        if (forEntity.getStatusCode().is2xxSuccessful()) {

            log.info(String.valueOf(forEntity.getStatusCode().value()));  //结果 int(200)
            log.info(forEntity.getStatusCode().toString());  //结果 string(200 OK)
            // 获取返回体
            return forEntity.getBody();
        }
        //返回失败状态码 int ,string
        return Output.failure(forEntity.getStatusCode().value(), forEntity.getStatusCode().toString());
    }

    @GetMapping("paymentLb")
    public String getPaymentLB() {
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        if (instances == null || instances.size() <= 0) {
            return null;
        }
        ServiceInstance serviceInstance = loadBalancer.instance(instances);
        URI uri = serviceInstance.getUri();
        return restTemplate.getForObject(uri + "/payment/port/", String.class);
    }

    @GetMapping("zipkin")
    public String paymentZipkin() {
        String result = restTemplate.getForObject(PAYMENT_URL + "/payment/zipkin/", String.class);
        return result;
    }
}
