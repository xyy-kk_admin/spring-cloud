package com.xyy.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author xyy
 * @date 2021年06月18日 10:51
 */
@Configuration
public class ApplicationContextConfig {
    @LoadBalanced  //使用@LoadBalanced赋予RestTemplate负载均衡能力
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}