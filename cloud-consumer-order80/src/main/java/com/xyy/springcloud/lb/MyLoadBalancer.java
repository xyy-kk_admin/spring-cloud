package com.xyy.springcloud.lb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author xyy
 * @date 2021年06月23日 9:04
 */
@Component
@Slf4j
public class MyLoadBalancer implements LoadBalancer {


    private AtomicInteger atomicInteger;

    // 初始化 创建一个原子类
    public MyLoadBalancer() {
        this.atomicInteger = new AtomicInteger(0);
    }

    @Override
    public ServiceInstance instance(List<ServiceInstance> serviceInstances) {
        // 当前访问次数 / 服务器数量  求  余数
        int index = getEndIncrement() % serviceInstances.size();
        //获取服务器的准确对象
        return serviceInstances.get(index);
    }

    //
    public final int getEndIncrement() {
        int current;    // atomicInteger目前的最大值,0开始
        int next;       // 访问的次数
        do {
            current = this.atomicInteger.get();
            next = current > Integer.MAX_VALUE ? 0 : current + 1;
            //如果当前值{@code ==}为期望值，则自动将该值设置为给定的更新值。
        } while (!this.atomicInteger.compareAndSet(current, next));
        log.info("******第几次访问? next: " + next);
        return next;
    }
}
