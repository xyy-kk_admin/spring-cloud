package com.xyy.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @author xyy
 * @date 2021年06月23日 9:02
 */
public interface LoadBalancer {
    ServiceInstance instance(List<ServiceInstance> serviceInstances);

}
