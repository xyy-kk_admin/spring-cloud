package com.xyy.springcloud.controller;

import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import com.xyy.springcloud.service.PaymentFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年06月23日 11:38
 */
@Slf4j
@RestController
@RequestMapping("/order/consumer/feign")
public class OrderFeignController {
    @Resource
    private PaymentFeignService paymentFeignService;

    @RequestMapping("/get/{id}")
    public Output<Payment> getPaymentById(@PathVariable("id") Long id) {
        return paymentFeignService.getPaymentById(id);
    }

    @GetMapping("timeout")
    public String paymentFeignTimeout() {
        return paymentFeignService.paymentFeignTimeout();
    }
}
