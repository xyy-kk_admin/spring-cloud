package com.xyy.springcloud.service;

import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author xyy
 * @date 2021年06月23日 11:31
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {
    String path = "/payment/";

    @GetMapping(path + "get/{id}")
    Output<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(path + "timeout")
    String paymentFeignTimeout();
}
