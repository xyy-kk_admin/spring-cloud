package com.xyy.springcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * HTTP请求，json输出格式定义
 *
 * @author xyy
 */
@Data
@ToString
@EqualsAndHashCode
public class Output<T> {
    private String message;
    private String errcode;
    private Boolean success;
    private T data;

    /**
     * 返回成功，不反数据
     *
     * @return 返回成功，不反数据
     */
    public static <T> Output<T> success() {
        final Output<T> output = new Output<>();
        output.setSuccess(true);
        output.setMessage("ok");
        return output;
    }

    /**
     * 返回失败
     *
     * @param message 失败消息
     * @return 返回失败
     */
    public static <T> Output<T> failure(String message) {
        final Output<T> output = new Output<>();
        output.setSuccess(false);
        output.setMessage(message);
        return output;
    }

    /**
     * 返回失败，返回代码和失败消息
     *
     * @param message 失败消息
     * @param errcode 失败代码
     * @return 返回代码和失败消息
     */
    public static <T> Output<T> failure(int errcode, String message) {
        final Output<T> output = new Output<>();
        output.setSuccess(false);
        output.setMessage(message);
        output.setErrcode(String.valueOf(errcode));
        return output;
    }

    /**
     * 返回成功，返回数据
     *
     * @param data 返回的数据
     * @return 返回成功，返回数据
     */
    public static <T> Output<T> success(T data) {
        final Output<T> output = new Output<>();
        output.setData(data);
        output.setSuccess(true);
        output.setMessage("ok");
        return output;
    }

    /**
     * 返回成功，返回数据,返回提示信息
     *
     * @param data    返回的数据
     * @param message 返回的提示信息
     * @return 返回成功，返回数据
     */
    public static <T> Output<T> success(String message, T data) {
        final Output<T> output = new Output<>();
        output.setData(data);
        output.setSuccess(true);
        output.setMessage(message);
        return output;
    }
}
