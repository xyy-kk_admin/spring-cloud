package com.xyy.springcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xyy.springcloud.entity.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author xyy
 * @since 2021-06-17
 */
@Mapper
public interface PaymentMapper extends BaseMapper<Payment> {
    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);
}
