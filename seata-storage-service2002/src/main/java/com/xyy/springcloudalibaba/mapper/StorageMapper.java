package com.xyy.springcloudalibaba.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author xyy
 * @date 2021年07月08日 15:59
 */
@Mapper
public interface StorageMapper {
    //扣减库存
    Integer decrease(@Param("productId") Long productId, @Param("count") Integer count);
}
