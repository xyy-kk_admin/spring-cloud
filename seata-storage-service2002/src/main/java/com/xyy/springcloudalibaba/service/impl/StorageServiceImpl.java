package com.xyy.springcloudalibaba.service.impl;

import com.xyy.springcloudalibaba.mapper.StorageMapper;
import com.xyy.springcloudalibaba.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年07月08日 16:06
 */
@Service
@Slf4j
public class StorageServiceImpl implements StorageService {

    @Resource
    private StorageMapper storageMapper;

    @Override
    public void decrease(Long productId, Integer count) {
        log.info("----->开始减库存");
        log.info("--->productId=" + productId + "  count=" + count);
        Integer result = storageMapper.decrease(productId, count);
        log.info("---->result=" + result);
        log.info("----->结束减库存");
    }
}
