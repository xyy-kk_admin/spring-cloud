package com.xyy.springcloudalibaba.service;

/**
 * @author xyy
 * @date 2021年07月08日 16:06
 */
public interface StorageService {
    /**
     * 扣减库存
     */
    void decrease(Long productId, Integer count);
}
