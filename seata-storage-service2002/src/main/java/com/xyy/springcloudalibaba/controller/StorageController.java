package com.xyy.springcloudalibaba.controller;

/**
 * @author xyy
 * @date 2021年07月08日 16:07
 */

import com.xyy.springcloudalibaba.entity.Output;
import com.xyy.springcloudalibaba.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("storage")
public class StorageController {

    @Autowired
    private StorageService storageService;

    //  扣减库存
    @PostMapping("decrease")
    public Output decrease(Long productId, Integer count) {
        storageService.decrease(productId, count);
        return Output.success("扣减库存成功！", null);
    }
}

