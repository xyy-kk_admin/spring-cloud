package com.xyy.springcloud.controller;

import com.xyy.springcloud.service.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年06月23日 14:25
 */
@RestController
@Slf4j
@RequestMapping("/payment/hystrix")
public class PaymentController {

    @Resource
    private IPaymentService iPaymentService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping("ok/{id}")
    public String paymentInfo_OK(@PathVariable("id") Integer id) {
        String result = iPaymentService.paymentInfo_OK(id);
        log.info(result);
        return result;
    }

    @GetMapping("timeout/{id}")
    public String paymentInfo_timeout(@PathVariable("id") Integer id) {
        String result = iPaymentService.paymentInfo_timeout(id);
        return result;
    }

    @GetMapping("circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id) {
        String result = iPaymentService.paymentCircuitBreaker(id);
        log.info("**********result = " + result);
        return result;
    }
}
