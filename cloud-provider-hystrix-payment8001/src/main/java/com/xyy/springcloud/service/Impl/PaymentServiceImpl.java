package com.xyy.springcloud.service.Impl;

import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.xyy.springcloud.service.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author xyy
 * @date 2021年06月23日 14:17
 */
@Service
@Slf4j
public class PaymentServiceImpl implements IPaymentService {

    //服务降级
    @Override
    public String paymentInfo_OK(Integer id) {
        return "线程池:   " + Thread.currentThread().getName() + "   paymentInfo_OK,id:   " + id;
    }


    //commandProperties,执行时间设置
    @HystrixCommand(fallbackMethod = "paymentInfo_timeoutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    })
    @Override
    public String paymentInfo_timeout(Integer id) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //int age = 10/0;
        return "线程池:   " + Thread.currentThread().getName() + "   paymentInfo_timeout,id:   " + id;
    }


    public String paymentInfo_timeoutHandler(Integer id) {
        return "线程池:   " + Thread.currentThread().getName() + "  系统繁忙请稍后再试,id:   " + id + "o(╥﹏╥)o";
    }


    //=====服务熔断
    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),// 失败率达到多少后跳闸
    })
    @Override
    // 开启断路器,在10秒内(时间窗口期),请求10次,失败率达到60%,跳闸
    public String paymentCircuitBreaker(Integer id) {
        //id为负数手动抛出异常
        if (id < 0) {
            throw new RuntimeException("******id 不能负数");
        }
        String serialNumber = IdUtil.simpleUUID();
        return Thread.currentThread().getName() + "\t" + "调用成功，流水号: " + serialNumber;
    }

    public String paymentCircuitBreaker_fallback(Integer id) {
        return "id 不能负数，请稍后再试，/(ㄒoㄒ)/~~   id: " + id;
    }

}
