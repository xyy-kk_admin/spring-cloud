package com.xyy.springcloud.service;

/**
 * @author xyy
 * @date 2021年06月23日 14:17
 */
public interface IPaymentService {
    String paymentInfo_OK(Integer id);

    String paymentInfo_timeout(Integer id);

    String paymentCircuitBreaker(Integer id);
}
