package com.xyy.springcloudalibaba.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xyy
 * @date 2021年07月05日 10:55
 */
@RestController
@RequestMapping("/config")
@RefreshScope  //支持nacos动态刷新功能
public class ConfigClientController {
    @Value("${config.info}")
    private String configInfo;

    @RequestMapping("/info")
    public String getConfigInfo() {
        return configInfo;
    }
}
