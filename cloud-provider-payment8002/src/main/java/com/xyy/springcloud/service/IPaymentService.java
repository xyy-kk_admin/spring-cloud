package com.xyy.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xyy.springcloud.entity.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author xyy
 * @since 2021-06-17
 */
public interface IPaymentService extends IService<Payment> {
    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);
}
