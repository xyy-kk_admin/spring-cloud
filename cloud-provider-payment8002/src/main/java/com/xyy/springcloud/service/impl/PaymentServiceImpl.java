package com.xyy.springcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyy.springcloud.entity.Payment;
import com.xyy.springcloud.mapper.PaymentMapper;
import com.xyy.springcloud.service.IPaymentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author xyy
 * @since 2021-06-17
 */
@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements IPaymentService {


    @Override
    public int create(Payment payment) {
        return super.getBaseMapper().create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return super.getBaseMapper().getPaymentById(id);
    }
}
