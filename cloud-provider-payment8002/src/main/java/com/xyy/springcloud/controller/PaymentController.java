package com.xyy.springcloud.controller;


import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import com.xyy.springcloud.service.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xyy
 * @since 2021-06-17
 */
@RestController
@Slf4j
@RequestMapping("/payment")
public class PaymentController {
    @Resource
    private IPaymentService iPaymentService;

    @Resource
    private DiscoveryClient discoveryClient;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping("create")
    @ResponseBody
    public Output create(@RequestBody Payment payment) {
        int result = iPaymentService.create(payment);
        log.info("****插入结果:" + result);
        if (result > 0) {
            return Output.success("插入成功+serverProt=" + serverPort, payment.getId());
        }
        return Output.failure(444, "插入失败");
    }

    @GetMapping("get/{id}")
    public Output getPaymentById(@PathVariable("id") Long id) {
        Payment paymentById = iPaymentService.getPaymentById(id);
        log.info("****查询id:" + paymentById + "=============");
        if (paymentById != null) {
            return Output.success("查询成功+serverProt=" + serverPort, paymentById);
        }
        return Output.failure(444, "查询失败" + id);
    }

    @GetMapping("discovery")
    public Object discovery() {
        List<String> services = discoveryClient.getServices();
        services.stream().forEach(System.out::println);
        System.out.println("====================================");
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances) {
            log.info(instance.getServiceId() + "\t" + instance.getHost() + "\t" + instance.getPort() + "\t" + instance.getUri());
        }
        return this.discoveryClient;
    }

    @RequestMapping("port")
    public String getPaymentPort() {
        return serverPort;
    }

    @RequestMapping("timeout")
    public String paymentFeignTimeout() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverPort;
    }

    @RequestMapping("zipkin")
    public String paymentZipkin() {
        return "paymentZipkin   (*^▽^*)port:  " + serverPort;
    }
}
