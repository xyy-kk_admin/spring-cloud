package com.xyy.springcloudalibaba.controller;

import cn.hutool.core.thread.ThreadUtil;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xyy
 * @date 2021年07月06日 10:24
 */
@RestController
@Slf4j
@RequestMapping("sentinel/flow")
public class FlowLimitController {

    @SentinelResource("getA")
    @RequestMapping("A")
    public String testA() {
        // ThreadUtil.sleep(800);
        return "---A";
    }

    @SentinelResource("getB")
    @RequestMapping("B")
    public String testB() {
        log.info(Thread.currentThread().getName() + "\t..........getB");
        return "---B";
    }

    @SentinelResource("getD")
    @RequestMapping("D")
    public String testD() {
        ThreadUtil.sleep(1000);
        log.info(Thread.currentThread().getName() + "\t..........getD");
        return "---D";
    }

    //deal_testHotKey 为降级服务名
    @SentinelResource(value = "testHotKey", blockHandler = "deal_testHotKey", fallback = "deal_testHotKey")
    @RequestMapping("testHotKey")

    public String testHotKey(@RequestParam(value = "p1", required = false) String p1,
                             @RequestParam(value = "p2", required = false) String p2) {
        int age = 10 / 0;
        return "---testHotKey";
    }

    public String deal_testHotKey(String p1, String p2, BlockException blockException) {
        log.info("---deal_testHotKey---o(╥﹏╥)o" + p1 + p2);
        return "---deal_testHotKey---o(╥﹏╥)o" + p1 + p2;
    }
}
