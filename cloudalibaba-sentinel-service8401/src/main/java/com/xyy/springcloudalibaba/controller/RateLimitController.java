package com.xyy.springcloudalibaba.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.xyy.springcloud.entity.Output;
import com.xyy.springcloud.entity.Payment;
import com.xyy.springcloudalibaba.myhandler.CustomerBlockHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xyy
 * @date 2021年07月07日 11:40
 */
@RestController
@RequestMapping("/sentinel/rate")
public class RateLimitController {

    @SentinelResource(value = "byResource", blockHandler = "handleException")
    @GetMapping("/byResource")
    public Output byResource() {
        return Output.success("按资源名称限流成功", new Payment(2020L, "serial001"));
    }

    public Output handleException(BlockException exception) {
        return Output.failure(444, exception.getClass().getCanonicalName() + "\t服务失败");
    }

    @SentinelResource(value = "byURL")
    @GetMapping("/byURL")
    public Output byURL() {
        return Output.success("按URL称限流成功", new Payment(2020L, "serial001"));
    }

    @SentinelResource(value = "customerBlockHandler",
            blockHandlerClass = CustomerBlockHandler.class,
            blockHandler = "handlerException2")
    @GetMapping("/customerBlockHandler/{id}")
    public Output byURL1(@PathVariable Integer id) {
        id++;
        System.out.println(id + "----------");
        return Output.success("自定义  限流成功", new Payment(2020L, "serial001"));
    }
}
