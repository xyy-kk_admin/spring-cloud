package com.xyy.springcloudalibaba.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.xyy.springcloud.entity.Output;

/**
 * @author xyy
 * @date 2021年07月07日 12:06
 */
public class CustomerBlockHandler {

    public static Output handlerException(BlockException exception) {
        return Output.failure(444, exception.getClass().getCanonicalName() + "\t客户自定义兜底方法=========1");
    }

    public static Output handlerException2(Integer id, BlockException exception) {
        System.out.println(id + "====================");
        return Output.failure(444, exception.getClass().getCanonicalName() + "\t客户自定义兜底方法==========2");
    }
}
