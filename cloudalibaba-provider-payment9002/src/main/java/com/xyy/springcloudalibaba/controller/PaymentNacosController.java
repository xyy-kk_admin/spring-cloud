package com.xyy.springcloudalibaba.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xyy
 * @date 2021年07月05日 9:43
 */
@RestController
@RequestMapping("payment/nacos")
public class PaymentNacosController {
    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "getport/{id}")
    public String getPayment(@PathVariable("id") Integer id) {
        return "nacos registry, serverPort: " + serverPort + "\t id" + id;
    }
}
