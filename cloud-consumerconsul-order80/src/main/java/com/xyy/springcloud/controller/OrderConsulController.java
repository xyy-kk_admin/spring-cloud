package com.xyy.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年06月22日 11:40
 */
@RestController
@Slf4j
@RequestMapping("consumer")
public class OrderConsulController {

    public static final String PAYMENT_URL = "http://cloud-consul-payment";

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("consul")
    public String paymentInfo() {
        String result = restTemplate.getForObject(PAYMENT_URL + "/payment/consul", String.class);
        return result;
    }
}
