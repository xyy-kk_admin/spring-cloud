package com.xyy.springcloud.service;

/**
 * @author xyy
 * @date 2021年07月01日 15:07
 */
public interface IMessageProvider {
    String sendInfo();
}
