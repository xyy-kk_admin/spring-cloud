package com.xyy.springcloud.service.Impl;

import cn.hutool.core.util.IdUtil;
import com.xyy.springcloud.service.IMessageProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;


/**
 * @author xyy
 * @date 2021年07月01日 15:08
 */
@EnableBinding(Source.class)    //定义消息推送管道
@Slf4j
public class MessageProviderImpl implements IMessageProvider {

    //不需要dao,操作数据库,需要的是:RabbitMQ消息中间件
    @Resource
    private MessageChannel output;     //消息发送管道

    @Override
    public String sendInfo() {
        String serial = IdUtil.simpleUUID();
        output.send(MessageBuilder.withPayload(serial).build());
        log.info("*********serial: " + serial);
        return serial;
    }
}
