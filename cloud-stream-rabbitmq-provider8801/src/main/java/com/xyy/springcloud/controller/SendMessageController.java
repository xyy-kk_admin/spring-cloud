package com.xyy.springcloud.controller;

import com.xyy.springcloud.service.IMessageProvider;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年07月01日 15:29
 */
@RestController
@RequestMapping("stream")
public class SendMessageController {

    @Resource
    private IMessageProvider iMessageProvider;

    @RequestMapping("sendMessage")
    public String sendMessage() {
        return iMessageProvider.sendInfo();
    }
}
