package com.xyy.springcloudalibaba.service.impl;

import com.xyy.springcloudalibaba.mapper.AccountMapper;
import com.xyy.springcloudalibaba.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author xyy
 * @date 2021年07月08日 16:18
 */
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    @Resource
    private AccountMapper accountMapper;

    /**
     * 扣减账户余额
     */
    @Override
    public void decrease(Long userId, BigDecimal money) {
        log.info("------->account-service中扣减账户余额开始");
        log.info("--->userId:" + userId + "   money:" + money);
        //模拟超时异常,全局事务回滚
        if (1 == 1) {
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常....");
        }
        Integer result = accountMapper.decrease(userId, money);
        log.info("---->result=" + result);
        log.info("------->account-service中扣减账户余额结束");
    }
}
