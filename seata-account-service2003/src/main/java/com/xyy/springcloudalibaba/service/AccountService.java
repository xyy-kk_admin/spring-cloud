package com.xyy.springcloudalibaba.service;


import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author xyy
 * @date 2021年07月08日 16:17
 */
public interface AccountService {

    /**
     * 扣减账户余额
     *
     * @param userId 用户id
     * @param money  金额
     */
    void decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}

