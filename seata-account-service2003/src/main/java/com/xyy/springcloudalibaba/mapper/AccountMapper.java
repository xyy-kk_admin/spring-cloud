package com.xyy.springcloudalibaba.mapper;

/**
 * @author xyy
 * @date 2021年07月08日 16:15
 */

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

@Mapper
public interface AccountMapper {

    // 扣减账户余额
    Integer decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);
}
