package com.xyy.springcloudalibaba.controller;

import com.xyy.springcloudalibaba.entity.Order;
import com.xyy.springcloudalibaba.entity.Output;
import com.xyy.springcloudalibaba.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xyy
 * @date 2021年07月08日 15:10
 */
@RestController
@RequestMapping("order")
public class OrderController {

    @Resource
    private OrderService orderService;

    //http://localhost:2001/order/create?userId=1&productId=1&cout=10&money=100
    @GetMapping("create")
    public Output create(Order order) {
        Long orderid = orderService.create(order);
        return Output.success("成功", "订单id: " + orderid);
    }
}
