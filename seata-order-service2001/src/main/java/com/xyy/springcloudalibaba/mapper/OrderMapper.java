package com.xyy.springcloudalibaba.mapper;

import com.xyy.springcloudalibaba.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author xyy
 * @date 2021年07月08日 14:35
 */
@Mapper
public interface OrderMapper {

    //1, 新建订单
    void create(Order order);

    //2. 修改订单状态,0-->1
    Integer update(@Param("id") Long id);
}
