package com.xyy.springcloudalibaba.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author xyy
 * @date 2021年07月08日 15:35
 */
@Configuration
@MapperScan("com.xyy.springcloudalibaba.mapper")
public class MyBatisConfig {
}
