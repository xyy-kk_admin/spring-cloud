package com.xyy.springcloudalibaba.service;

import com.xyy.springcloudalibaba.entity.Output;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author xyy
 * @date 2021年07月08日 14:46
 */
@FeignClient(value = "seata-storage-service")
public interface StorageFeignService {

    @PostMapping(value = "/storage/decrease")
    Output decrease(@RequestParam("productId") Long productId, @RequestParam("count") Integer count);

}