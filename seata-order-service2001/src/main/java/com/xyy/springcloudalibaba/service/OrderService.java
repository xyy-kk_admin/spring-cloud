package com.xyy.springcloudalibaba.service;

import com.xyy.springcloudalibaba.entity.Order;

/**
 * 订单类
 *
 * @author xyy
 * @date 2021年07月08日 14:43
 */
public interface OrderService {
    Long create(Order order);
}
