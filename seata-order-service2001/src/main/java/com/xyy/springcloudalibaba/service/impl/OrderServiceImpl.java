package com.xyy.springcloudalibaba.service.impl;

import com.xyy.springcloudalibaba.entity.Order;
import com.xyy.springcloudalibaba.mapper.OrderMapper;
import com.xyy.springcloudalibaba.service.AccountFeignService;
import com.xyy.springcloudalibaba.service.OrderService;
import com.xyy.springcloudalibaba.service.StorageFeignService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderMapper orderMapper;
    @Resource
    private StorageFeignService storageFeignService;
    @Resource
    private AccountFeignService accountFeignService;

    /**
     * 创建订单->调用库存服务扣减库存->调用账户服务扣减账户余额->修改订单状态
     * 简单说：下订单->扣库存->减余额->改状态
     */
    @Override
    @GlobalTransactional
    public Long create(Order order) {
        log.info("----->开始新建订单");
        //1 新建订单
        orderMapper.create(order);
        log.info("----->订单id为:" + order.getId());
        //2 扣减库存
        log.info("----->订单微服务开始调用库存，做扣减Count");
        storageFeignService.decrease(order.getProductId(), order.getCount());
        log.info("----->订单微服务开始调用库存，做扣减end");

        //3 扣减账户
        log.info("----->订单微服务开始调用账户，做扣减Money");
        accountFeignService.decrease(order.getUserId(), order.getMoney());

        log.info("----->订单微服务开始调用账户，做扣减end");

        //4 修改订单状态，从零到1,1代表已经完成
        log.info("----->修改订单状态开始");
        orderMapper.update(order.getId());
        log.info("----->修改订单状态结束");

        log.info("----->下订单结束了，O(∩_∩)O哈哈~");
        return order.getId();
    }
}
