package com.xyy.springcloudalibaba.service;

import com.xyy.springcloudalibaba.entity.Output;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author xyy
 * @date 2021年07月08日 14:46
 */
@FeignClient(value = "seata-account-service")
public interface AccountFeignService {

    @PostMapping(value = "/account/decrease")
    Output decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}
